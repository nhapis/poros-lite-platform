#! /bin/bash

ERROR_CODE=10
SUCCESS_CODE=0

#打印日志函数
function log_output(){

	DATE=`date "+%Y-%m-%d %H:%M:%S"`
	echo ${DATE} $1
	echo ${DATE} $1 >> ${LOG_FILE}
	return ${SUCCESS_CODE}
}

#读取配置文件
function ReadConfig(){
	while read line
	do
		if [  -z `echo ${line} | grep "^\["` ];then
			eval "${line}"
		fi
	done < config.ini
}

#部署Zookeeper
function DeployZK(){

	ZOOKEEPER_TOOLS_PATH=${POROS_DIR}/zookeeper-3.4.14
	
	log_output 'Zookeeper 部署目录：'${ZOOKEEPER_TOOLS_PATH}
	log_output 'Zookeeper 数据目录：'${ZOOKEEPER_DATA_PATH}
	mkdir -p ${POROS_DIR} > /dev/null 2>&1
	mkdir -p ${ZOOKEEPER_DATA_PATH} > /dev/null 2>&1
	mkdir -p ${ZOOKEEPER_TOOLS_PATH} > /dev/null 2>&1
	scp -r ./poros-litePackage/zookeeper-3.4.14.tar.gz ${POROS_DIR}/ > /dev/null 2>&1
	rm -rf ${ZOOKEEPER_TOOLS_PATH} > /dev/null 2>&1
	tar -xvf ${POROS_DIR}/zookeeper-3.4.14.tar.gz -C ${POROS_DIR}/ > /dev/null 2>&1
	mkdir -p ${ZOOKEEPER_TOOLS_PATH}/logs > /dev/null 2>&1
	rm -rf ${POROS_DIR}/zookeeper-3.4.14.tar.gz > /dev/null 2>&1

	scp -r ./poros-conf/zookeeper/conf/* ${ZOOKEEPER_TOOLS_PATH}/conf/ > /dev/null 2>&1
	scp -r ./poros-conf/zookeeper/dataDir ${ZOOKEEPER_DATA_PATH}/ > /dev/null 2>&1
	scp -r ./poros-conf/zookeeper/bin/* ${ZOOKEEPER_TOOLS_PATH}/bin/ > /dev/null 2>&1
	scp -r ./poros-conf/zookeeper/lib/* ${ZOOKEEPER_TOOLS_PATH}/lib/ > /dev/null 2>&1
	
	sed -i "s#nhApis_LOGS#${ZOOKEEPER_TOOLS_PATH}/logs#g" ${ZOOKEEPER_TOOLS_PATH}/bin/zkEnv.sh
	sed -i "s#nhApis_ZOOKEEPER_TOOLS_PATH#${ZOOKEEPER_TOOLS_PATH}#g" ${ZOOKEEPER_TOOLS_PATH}/bin/zkEnv.sh
	sed -i "s#nhApis_ZOOKEEPER_TOOLS_PATH#${ZOOKEEPER_TOOLS_PATH}#g" ${ZOOKEEPER_TOOLS_PATH}/bin/start-zk.sh
	sed -i "s#nhApis_ZOOKEEPER_TOOLS_PATH#${ZOOKEEPER_TOOLS_PATH}#g" ${ZOOKEEPER_TOOLS_PATH}/bin/stop-zk.sh
	chmod 755 ${ZOOKEEPER_TOOLS_PATH}/bin/start-zk.sh
	chmod 755 ${ZOOKEEPER_TOOLS_PATH}/bin/stop-zk.sh
	sed -i "s#nhApis_ZOOKEEPER_TOOLS_PATH#${ZOOKEEPER_TOOLS_PATH}#g" ${ZOOKEEPER_TOOLS_PATH}/bin/status-zk.sh
	chmod 755 ${ZOOKEEPER_TOOLS_PATH}/bin/status-zk.sh
	sed -i "s#nhApis_ZOOKEEPER_DATA_PATH#${ZOOKEEPER_DATA_PATH}#g" ${ZOOKEEPER_TOOLS_PATH}/conf/zoo1.cfg
	sed -i "s#nhApis_WORK_IP#${WORK_IP}#g" ${ZOOKEEPER_TOOLS_PATH}/conf/zoo1.cfg
	sed -i "s#nhApis_ZOOKEEPER_DATA_PATH#${ZOOKEEPER_DATA_PATH}#g" ${ZOOKEEPER_TOOLS_PATH}/conf/zoo2.cfg
	sed -i "s#nhApis_WORK_IP#${WORK_IP}#g" ${ZOOKEEPER_TOOLS_PATH}/conf/zoo2.cfg
	sed -i "s#nhApis_ZOOKEEPER_DATA_PATH#${ZOOKEEPER_DATA_PATH}#g" ${ZOOKEEPER_TOOLS_PATH}/conf/zoo3.cfg
	sed -i "s#nhApis_WORK_IP#${WORK_IP}#g" ${ZOOKEEPER_TOOLS_PATH}/conf/zoo3.cfg
	
	log_output 'Zookeeper 地址/端口：'${WORK_IP}':22181,'${WORK_IP}':22182,'${WORK_IP}':22183'
	log_output 'Zookeeper 启动脚本：sh '${ZOOKEEPER_TOOLS_PATH}/bin/start-zk.sh
	sh ${ZOOKEEPER_TOOLS_PATH}/bin/start-zk.sh
	log_output 'Zookeeper 查看状态脚本：sh '${ZOOKEEPER_TOOLS_PATH}/bin/status-zk.sh
	sh ${ZOOKEEPER_TOOLS_PATH}/bin/status-zk.sh
	log_output 'Zookeeper 停止脚本：sh '${ZOOKEEPER_TOOLS_PATH}/bin/stop-zk.sh
}

#主函数
function main(){

	echo -e "\033[1m"
			
	#读取配置文件
	ReadConfig
	
	log_output '-----------------------------------------------'
	
	echo -e "\033[31m正在部署 \033[33mZookeeper-3.4.14,请稍后...\033[1m"
		  
	#部署Zookeeper
	DeployZK
   
	echo -e "\033[31m部署完成 \033[32mZookeeper-3.4.14部署完成. \033[1m"
	
	log_output '-----------------------------------------------'
	
	echo -e "\033[0m"
}

main

