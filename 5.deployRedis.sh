#!/bin/bash

ERROR_CODE=10
SUCCESS_CODE=0

#打印日志函数
function log_output(){

	DATE=`date "+%Y-%m-%d %H:%M:%S"`
	echo ${DATE} $1
	echo ${DATE} $1 >> ${LOG_FILE}
	return ${SUCCESS_CODE}
}

#读取配置文件
function ReadConfig(){
	while read line
	do
		if [  -z `echo ${line} | grep "^\["` ];then
			eval "${line}"
		fi
	done < config.ini
}

#安装redis
function InstallRedis(){

	mkdir -p ${POROS_DIR} > /dev/null 2>&1
	log_output 'Redis 密码：'${REDIS_PASSWORD}
	rpm -ivh ./poros-litePackage/redis/epel-release-7-11.noarch.rpm > /dev/null 2>&1
	rpm -ivh ./poros-litePackage/redis/jemalloc-3.6.0-1.el7.x86_64.rpm > /dev/null 2>&1
	rpm -ivh ./poros-litePackage/redis/redis-3.2.12-2.el7.x86_64.rpm > /dev/null 2>&1
	cp -av  ./poros-conf/redis/redis.conf /etc/redis.conf > /dev/null 2>&1
	sed -i "s#nhapis_redis_password#${REDIS_PASSWORD}#g" /etc/redis.conf
	sed -i "s#nhapis_redis_port#6379#g" /etc/redis.conf
	chown redis:root /etc/redis.conf
	log_output 'Redis 启动脚本：/usr/bin/redis-server /etc/redis.conf'
	/usr/bin/redis-server /etc/redis.conf
}

function main()
{
		echo -e "\033[1m"
		
		#读取配置文件
        ReadConfig
		
		log_output '-----------------------------------------------'
	
		echo -e "\033[31m正在部署 \033[33mRedis-3.2.12,请稍后...\033[1m"
     
		#安装redis
		InstallRedis
		
		echo -e "\033[31m部署完成 \033[32mRedis-3.2.12部署完成. \033[1m"
	
	    log_output '-----------------------------------------------'
		
		echo -e "\033[0m"		
}

main

