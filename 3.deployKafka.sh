#! /bin/bash

ERROR_CODE=10
SUCCESS_CODE=0

#打印日志函数
function log_output(){

	DATE=`date "+%Y-%m-%d %H:%M:%S"`
	echo ${DATE} $1
	echo ${DATE} $1 >> ${LOG_FILE}
	return ${SUCCESS_CODE}
}

#读取配置文件
function ReadConfig(){
	while read line
	do
		if [  -z `echo ${line} | grep "^\["` ];then
			eval "${line}"
		fi
	done < config.ini
}

#部署Kafka
function DeployKafka(){

	KAFKA_TOOLS_PATH=${POROS_DIR}/kafka_2.11-2.2.2
	ZOOKEEPER_TOOLS_PATH=${POROS_DIR}/zookeeper-3.4.14
	
	log_output 'Kafka 部署目录：'${KAFKA_TOOLS_PATH}
	log_output 'Kafka 数据目录：'${KAFKA_DATA_PATH}
	mkdir -p ${POROS_DIR} > /dev/null 2>&1
	mkdir -p ${KAFKA_DATA_PATH}/kafkaData1 > /dev/null 2>&1
	mkdir -p ${KAFKA_DATA_PATH}/kafkaData2 > /dev/null 2>&1
	mkdir -p ${KAFKA_DATA_PATH}/kafkaData3 > /dev/null 2>&1

	mkdir -p ${KAFKA_TOOLS_PATH} > /dev/null 2>&1
	scp -r ./poros-litePackage/kafka_2.11-2.2.2.tgz ${POROS_DIR}/ > /dev/null 2>&1
	rm -rf ${KAFKA_TOOLS_PATH} > /dev/null 2>&1
	tar -xvf ${POROS_DIR}/kafka_2.11-2.2.2.tgz -C ${POROS_DIR}/ > /dev/null 2>&1
	rm -rf ${POROS_DIR}/kafka_2.11-2.2.2.tgz > /dev/null 2>&1

	scp -r ./poros-conf/kafka/config/* ${KAFKA_TOOLS_PATH}/config/ > /dev/null 2>&1
	scp -r ./poros-conf/kafka/bin/* ${KAFKA_TOOLS_PATH}/bin/ > /dev/null 2>&1
	
	sed -i "s#nhApis_KAFKA_TOOLS_PATH#${KAFKA_TOOLS_PATH}#g" ${KAFKA_TOOLS_PATH}/bin/start-kafka.sh
	sed -i "s#nhApis_KAFKA_TOOLS_PATH#${KAFKA_TOOLS_PATH}#g" ${KAFKA_TOOLS_PATH}/bin/kafka-server-start.sh
	sed -i "s#nhApis_KAFKA_TOOLS_PATH#${KAFKA_TOOLS_PATH}#g" ${KAFKA_TOOLS_PATH}/bin/kafka-console-consumer.sh
	sed -i "s#nhApis_KAFKA_TOOLS_PATH#${KAFKA_TOOLS_PATH}#g" ${KAFKA_TOOLS_PATH}/bin/kafka-console-producer.sh
	chmod 755 ${KAFKA_TOOLS_PATH}/bin/start-kafka.sh
	chmod 755 ${KAFKA_TOOLS_PATH}/bin/kafka-server-start.sh
	chmod 755 ${KAFKA_TOOLS_PATH}/bin/kafka-console-consumer.sh
	chmod 755 ${KAFKA_TOOLS_PATH}/bin/kafka-console-producer.sh
	sed -i "s#nhApis_KAFKA_TOOLS_PATH#${KAFKA_TOOLS_PATH}#g" ${KAFKA_TOOLS_PATH}/bin/testAll.sh
	sed -i "s#nhApis_WORK_IP#${WORK_IP}#g" ${KAFKA_TOOLS_PATH}/bin/testAll.sh
	sed -i "s#nhApis_KAFKA_DATA_PATH#${KAFKA_DATA_PATH}#g" ${KAFKA_TOOLS_PATH}/config/server-1.properties
	sed -i "s#nhApis_WORK_IP#${WORK_IP}#g" ${KAFKA_TOOLS_PATH}/config/server-1.properties
	sed -i "s#nhApis_KAFKA_DATA_PATH#${KAFKA_DATA_PATH}#g" ${KAFKA_TOOLS_PATH}/config/server-2.properties
	sed -i "s#nhApis_WORK_IP#${WORK_IP}#g" ${KAFKA_TOOLS_PATH}/config/server-2.properties
	sed -i "s#nhApis_KAFKA_DATA_PATH#${KAFKA_DATA_PATH}#g" ${KAFKA_TOOLS_PATH}/config/server-3.properties
	sed -i "s#nhApis_WORK_IP#${WORK_IP}#g" ${KAFKA_TOOLS_PATH}/config/server-3.properties
	
	log_output 'Kafka Zookeeper配置目录：'${WORK_IP}':22181,'${WORK_IP}':22182,'${WORK_IP}':22183/KafkaRoot'
	log_output 'Kafka 地址/端口：'${WORK_IP}':29902,'${WORK_IP}':29903,'${WORK_IP}':29904'
	log_output 'Kafka 启动脚本：sh '${KAFKA_TOOLS_PATH}/bin/start-kafka.sh
	
	sh ${KAFKA_TOOLS_PATH}/bin/start-kafka.sh
	sleep 10
	sed -i "s#nhApis_WORK_IP#${WORK_IP}#g" ${KAFKA_TOOLS_PATH}/bin/autoAcl.sh
	sed -i "s#nhApis_ZOOKEEPER_TOOLS_PATH#${ZOOKEEPER_TOOLS_PATH}#g" ${KAFKA_TOOLS_PATH}/bin/autoAcl.sh
	chmod 755 ${KAFKA_TOOLS_PATH}/bin/autoAcl.sh
	sh ${KAFKA_TOOLS_PATH}/bin/autoAcl.sh
}

#主函数
function main(){

	echo -e "\033[1m"
			
	#读取配置文件
	ReadConfig
	
	log_output '-----------------------------------------------'
	
	echo -e "\033[31m正在部署 \033[33mKafka_2.11-2.2.2,请稍后...\033[1m"
		  
	#部署Kafka
	DeployKafka
   
	echo -e "\033[31m部署完成 \033[32mKafka_2.11-2.2.2部署完成. \033[1m"
	
	log_output '-----------------------------------------------'
	
	echo -e "\033[0m"
}

main

