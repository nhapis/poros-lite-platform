#! /bin/bash

ERROR_CODE=10
SUCCESS_CODE=0

#打印日志函数
function log_output(){

	DATE=`date "+%Y-%m-%d %H:%M:%S"`
	echo ${DATE} $1
	echo ${DATE} $1 >> ${LOG_FILE}
	return ${SUCCESS_CODE}
}

#读取配置文件
function ReadConfig(){
	while read line
	do
		if [  -z `echo ${line} | grep "^\["` ];then
			eval "${line}"
		fi
	done < config.ini
}

function initENV(){
	#禁止交换
    cp -rf /etc/sysctl.conf /etc/sysctl.conf.bak > /dev/null 2>&1
	echo "vm.swappiness=1" >> /etc/sysctl.conf
    echo "vm.max_map_count=262144" >> /etc/sysctl.conf
    sysctl -p > /dev/null 2>&1

			#文件访问数量
	cp -rf /etc/security/limits.conf /etc/security/limits.conf.bak > /dev/null 2>&1
	echo '* soft nofile 655350' >> /etc/security/limits.conf
    echo '* hard nofile 655350' >> /etc/security/limits.conf
    echo '* soft nproc 655350' >> /etc/security/limits.conf
    echo '* hard nproc 655350' >> /etc/security/limits.conf
	sed -i 's/4096/40960/g' /etc/security/limits.d/20-nproc.conf
}

function selinuxMod(){
	setenforce 0
	sed -i 's/=enforcing/=disabled/g' /etc/selinux/config
}

function openFirewall(){
	systemctl enable firewalld.service
	service firewalld restart
	firewall-cmd --list-all
	firewall-cmd --permanent --add-port=80/tcp
	firewall-cmd --permanent --add-port=8048/tcp
	firewall-cmd --permanent --add-port=29902/tcp
	firewall-cmd --permanent --add-port=29903/tcp
	firewall-cmd --permanent --add-port=29904/tcp
	firewall-cmd --permanent --add-port=28801/tcp
	firewall-cmd --permanent --add-port=25610/tcp
	firewall-cmd --permanent --add-port=6669/udp
	firewall-cmd --reload
	firewall-cmd --list-all
}

#部署JDK
function DeployJDK(){

	log_output '工作目录：'${POROS_DIR}
	log_output 'JDK目录：'${POROS_DIR}/jdk1.8.0_261
	mkdir -p ${POROS_DIR} > /dev/null 2>&1
	scp -r ./poros-litePackage/jdk-8u261-linux-x64.tar.gz ${POROS_DIR}/ > /dev/null 2>&1
	tar -xvf ${POROS_DIR}/jdk-8u261-linux-x64.tar.gz -C ${POROS_DIR}/ > /dev/null 2>&1
	rm -rf ${POROS_DIR}/jdk-8u261-linux-x64.tar.gz > /dev/null 2>&1

	echo "export JAVA_HOME=$POROS_DIR/jdk1.8.0_261" >> /etc/profile
	echo "export JRE_HOME=\$JAVA_HOME/jre" >> /etc/profile
	echo "export PATH=\$JAVA_HOME/bin:\$PATH:\$JRE_HOME/lib" >> /etc/profile
	echo "export CLASSPATH=.:\$JAVA_HOME/lib:\$JRE_HOME/lib:\$CLASSPATH" >> /etc/profile
	echo "export PATH=\$PATH:\$JAVA_HOME/bin" >> /etc/profile
	#echo "JAVA_HOME=$POROS_DIR/jdk1.8.0_261" >> /etc/environment
	source /etc/profile 
	echo "source /etc/profile" >> ~/.bashrc
	echo -e "\033[31m正在配置环境变量 \033[33m请稍后... \033[1m"
	 
}

#主函数
function main(){

	echo -e "\033[1m"
	
	#读取配置文件
	ReadConfig
	
	log_output '-----------------------------------------------'
	
	echo -e "\033[31m正在配置 \033[33m文件打开数量、禁止内存交换、防火墙策略，请稍后...\033[1m"
		  
	initENV	  
	selinuxMod
	openFirewall
	
	echo -e "\033[31m配置完成 \033[32m文件打开数量、禁止内存交换、防火墙策略. \033[1m"
	
	echo -e "\033[31m正在部署 \033[33mjdk1.8.0_261,请稍后...\033[1m"
	#部署JDK
	DeployJDK
   
	echo -e "\033[31m部署完成 \033[32mjdk1.8.0_261部署完成. \033[1m"
	
	log_output '-----------------------------------------------'
	
	echo -e "\033[32m基础配置完毕,需要重启一下服务器再进行下一步操作. \033[1m"
	echo -e "\033[32m基础配置完毕,需要重启一下服务器再进行下一步操作. \033[1m"
	echo -e "\033[32m基础配置完毕,需要重启一下服务器再进行下一步操作. \033[1m"
	echo -e "\033[32m重要的事情说三遍. \033[1m"
	
	echo -e "\033[0m"
}

main

