#!/bin/bash

ERROR_CODE=10
SUCCESS_CODE=0

LOCAL_WORK_PATH=`echo $( cd "$( dirname "${BASH_SOURCE[0]}" )/" && pwd )`


#打印日志函数
function log_output(){

	DATE=`date "+%Y-%m-%d %H:%M:%S"`
	echo ${DATE} $1
	echo ${DATE} $1 >> ${LOG_FILE}
	return ${SUCCESS_CODE}
}

#读取配置文件
function ReadConfig(){
	while read line
	do
		if [  -z `echo ${line} | grep "^\["` ];then
			eval "${line}"
		fi
	done < config.ini
}

function DeployNginx(){
	mkdir -p ${POROS_DIR} > /dev/null 2>&1
	NGINX_PATH=${POROS_DIR}/nginx
	echo -e "\033[31m正在安装必要的Yum包 \033[33mgcc openssl-devel pcre-devel zlib-devel ,请稍后...\033[1m"	
	yum -y install gcc openssl-devel pcre-devel zlib-devel 
	tar -xvf ./poros-litePackage/tengine-2.3.2.tar.gz -C ${POROS_DIR}/
	cd $LOCAL_WORK_PATH && log_output 'Nginx 部署目录：'${NGINX_PATH}
	echo -e "\033[31m正在编译 \033[33mNginx ,请稍后...\033[1m"	
	cd ${POROS_DIR}/tengine-2.3.2 && ./configure --prefix=${NGINX_PATH} --error-log-path=${NGINX_PATH}/logs/error.log --http-log-path=${NGINX_PATH}/logs/access.log --pid-path=${NGINX_PATH}/run/nginx.pid --lock-path=${NGINX_PATH}/run/nginx.lock --with-http_ssl_module --with-http_flv_module --with-http_stub_status_module --with-http_gzip_static_module --http-client-body-temp-path=${NGINX_PATH}/tmp/client/ --http-proxy-temp-path=${NGINX_PATH}/tmp/proxy/ --http-fastcgi-temp-path=${NGINX_PATH}/tmp/fcgi/ --http-uwsgi-temp-path=${NGINX_PATH}/tmp/uwsgi --http-scgi-temp-path=${NGINX_PATH}/tmp/scgi --with-pcre 
	cd ${POROS_DIR}/tengine-2.3.2 && make && make install 
	rm -rf ${NGINX_PATH}/html/index.html
	NOW_DATE=`date "+%Y-%m-%d %H:%M:%S"`
	echo "<h4> Deploy Time: $NOW_DATE</h4><h1>Welcome to the world of Poros!</h1>" >> ${NGINX_PATH}/html/index.html
	mkdir -p ${NGINX_PATH}/tmp/client
	cd $LOCAL_WORK_PATH && log_output 'Nginx 启动脚本：'${NGINX_PATH}/sbin/nginx 
	${NGINX_PATH}/sbin/nginx 
	cd $LOCAL_WORK_PATH && log_output 'Nginx 重启脚本：'"${NGINX_PATH}/sbin/nginx -s reload" 
	rm -rf ${POROS_DIR}/tengine-2.3.2 > /dev/null 2>&1
	cd $LOCAL_WORK_PATH
}

function main()
{
	
	echo -e "\033[1m"
 	#读取配置文件
    ReadConfig
	

	log_output '-----------------------------------------------'

	echo -e "\033[31m正在部署 \033[33mNginx,请稍后...\033[1m"
		
	#安装Tengine
	DeployNginx
	
	echo -e "\033[31m部署完成 \033[32mNginx部署完成. \033[1m"

	log_output '-----------------------------------------------'
	
	echo -e "\033[0m"
}

main
