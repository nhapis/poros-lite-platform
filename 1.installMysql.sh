#! /bin/bash

ERROR_CODE=10
SUCCESS_CODE=0

#打印日志函数
function log_output(){

	DATE=`date "+%Y-%m-%d %H:%M:%S"`
	echo ${DATE} $1
	echo ${DATE} $1 >> ${LOG_FILE}
	return ${SUCCESS_CODE}
}

#读取配置文件
function ReadConfig(){
	while read line
	do
		if [  -z `echo ${line} | grep "^\["` ];then
			eval "${line}"
		fi
	done < config.ini
}

function Deploy(){

	echo -e "\033[31m正在安装yum包 \033[33mlibaio,请稍后... \033[1m"
	yum -y install libaio
	rpm -ivh ./poros-litePackage/mysql_5.7/mysql-community-server-5.7.31-1.el7.x86_64.rpm --force --nodeps
	rpm -ivh ./poros-litePackage/mysql_5.7/mysql-community-client-5.7.31-1.el7.x86_64.rpm --force --nodeps
	rpm -ivh ./poros-litePackage/mysql_5.7/mysql-community-devel-5.7.31-1.el7.x86_64.rpm --force --nodeps
	rpm -ivh ./poros-litePackage/mysql_5.7/mysql-community-common-5.7.31-1.el7.x86_64.rpm --force --nodeps
	rpm -ivh ./poros-litePackage/mysql_5.7/mysql-community-libs-5.7.31-1.el7.x86_64.rpm --force --nodeps
	rpm -ivh ./poros-litePackage/mysql_5.7/mysql-community-libs-compat-5.7.31-1.el7.x86_64.rpm --force --nodeps
	mv /etc/my.cnf /etc/my.cnf.bak
	mkdir -p ${POROS_DIR}/tmp
	scp -r ./poros-conf/mysql/* ${POROS_DIR}/tmp
	sed -i "s/nhapis_mysql_port/${MYSQL_PORT}/g" ${POROS_DIR}/tmp/my.cnf
	scp -r ${POROS_DIR}/tmp/my.cnf /etc/my.cnf
	systemctl enable mysqld.service
	systemctl start mysqld.service
	echo -e "\033[31m初始化配置 \033[33m设置密码、访问权限 \033[1m"
	PASSWORD=`grep 'temporary password' /var/log/mysqld.log|awk -F root@ '{print $2}'|awk '{print $2}'`
	PASSWORD=`echo "${PASSWORD// /}"`
	mysqladmin -uroot -p${PASSWORD} -P${MYSQL_PORT} password 'nhApis@123456'
	sed -i "s/nhapis_mysql_password/${MYSQL_PASS}/g" ${POROS_DIR}/tmp/config.sql
	mysql -uroot -pnhApis@123456 -P${MYSQL_PORT} < ${POROS_DIR}/tmp/config.sql
	log_output 'MySql 端口：'${MYSQL_PORT}
	log_output 'MySql 账号/密码：root/'${MYSQL_PASS}
	log_output 'MySql 启动命令：systemctl start mysqld.service'
	log_output 'MySql 重启命令：systemctl restart mysqld.service'
	systemctl restart mysqld.service
	rm -rf ${POROS_DIR}/tmp
}

function selinuxMod(){
	setenforce 0
	sed -i 's/=enforcing/=disabled/g' /etc/selinux/config
}

#主函数
function main(){

	echo -e "\033[1m"
			
	#读取配置文件
	ReadConfig
	
	selinuxMod
	
	log_output '-----------------------------------------------'
	
	echo -e "\033[31m正在部署 \033[33mMysql 5.7.31,请稍后...\033[1m"
		  	
	Deploy
   
	echo -e "\033[31m部署完成 \033[32mMysql 5.7.31部署完成. \033[1m"
	
	log_output '-----------------------------------------------'
	
	echo -e "\033[0m"
}

main

