#! /bin/bash

ERROR_CODE=10
SUCCESS_CODE=0

#打印日志函数
function log_output(){

	DATE=`date "+%Y-%m-%d %H:%M:%S"`
	echo ${DATE} $1
	echo ${DATE} $1 >> ${LOG_FILE}
	return ${SUCCESS_CODE}
}

#读取配置文件
function ReadConfig(){
	while read line
	do
		if [  -z `echo ${line} | grep "^\["` ];then
			eval "${line}"
		fi
	done < config.ini
}


function Deploy(){

	KIBANA_TOOLS_PATH=${POROS_DIR}/kibana-7.3.2
	
	rm -rf ${KIBANA_TOOLS_PATH} > /dev/null 2>&1
	mkdir -p ${POROS_DIR} > /dev/null 2>&1
	scp -r ./poros-litePackage/kibana-7.3.2-linux-x86_64.tar.gz ${POROS_DIR}/ > /dev/null 2>&1
	tar -xvf ${POROS_DIR}/kibana-7.3.2-linux-x86_64.tar.gz -C ${POROS_DIR}/ > /dev/null 2>&1
	log_output 'Kibana 部署目录：'${KIBANA_TOOLS_PATH}
	rm -rf ${POROS_DIR}/kibana-7.3.2-linux-x86_64.tar.gz > /dev/null 2>&1
	mv ${POROS_DIR}/kibana-7.3.2-linux-x86_64 ${KIBANA_TOOLS_PATH} > /dev/null 2>&1
	mkdir -p ${KIBANA_TOOLS_PATH}/logs > /dev/null 2>&1
	scp -r ./poros-conf/kibana/config/kibana.yml ${KIBANA_TOOLS_PATH}/config/ > /dev/null 2>&1
	echo "nohup ${KIBANA_TOOLS_PATH}/bin/kibana  --allow-root > ${KIBANA_TOOLS_PATH}/logs/run.logs 2>&1 &" >> ${KIBANA_TOOLS_PATH}/bin/start.sh
	chmod 755 ${KIBANA_TOOLS_PATH}/bin/start.sh
	log_output 'Kibana 启动脚本：'${KIBANA_TOOLS_PATH}/bin/start.sh
	${KIBANA_TOOLS_PATH}/bin/start.sh
	log_output "Kibana Web访问地址：http://${WORK_IP}:25610"
}

#主函数
function main(){

	echo -e "\033[1m"
			
	#读取配置文件
	ReadConfig
	
	log_output '-----------------------------------------------'
	
	echo -e "\033[31m正在部署 \033[33mKibana-7.3.2,请稍后...\033[1m"
		  
	Deploy
   
	echo -e "\033[31m部署完成 \033[32mKibana-7.3.2部署完成. \033[1m"
	
	log_output '-----------------------------------------------'
	
	echo -e "\033[0m"
}

main

