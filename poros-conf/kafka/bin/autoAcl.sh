#!/bin/sh
sh nhApis_ZOOKEEPER_TOOLS_PATH/bin/zkCli.sh -server nhApis_WORK_IP:22181,nhApis_WORK_IP:22182,nhApis_WORK_IP:22183 > /dev/null 2>&1 <<EOF
ls /
setAcl /zookeeper ip:nhApis_WORK_IP:cdrwa,ip:127.0.0.1:cdrwa
getAcl /zookeeper
setAcl /KafkaRoot ip:nhApis_WORK_IP:cdrwa,ip:127.0.0.1:cdrwa
getAcl /KafkaRoot
quit
EOF
