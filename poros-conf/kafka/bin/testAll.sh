#查看topic
sh nhApis_KAFKA_TOOLS_PATH/bin/kafka-topics.sh --list --zookeeper nhApis_WORK_IP:22181,nhApis_WORK_IP:22182,nhApis_WORK_IP:22183/KafkaRoot

#创建topic
sh nhApis_KAFKA_TOOLS_PATH/bin/kafka-topics.sh --zookeeper nhApis_WORK_IP:22181,nhApis_WORK_IP:22182,nhApis_WORK_IP:22183/KafkaRoot --create --topic nh-test --partitions 3 --replication-factor 2

#创建生产者
sh nhApis_KAFKA_TOOLS_PATH/bin/kafka-console-producer.sh --broker-list nhApis_WORK_IP:29902,nhApis_WORK_IP:29903,nhApis_WORK_IP:29904 --topic nh-test --producer.config nhApis_KAFKA_TOOLS_PATH/config/producer.properties

#创建消费者
sh nhApis_KAFKA_TOOLS_PATH/bin/kafka-console-consumer.sh --bootstrap-server nhApis_WORK_IP:29902,nhApis_WORK_IP:29903,nhApis_WORK_IP:29904 --topic nh-test  --from-beginning --consumer.config nhApis_KAFKA_TOOLS_PATH/config/consumer.properties