set password=password('nhapis_mysql_password');
set global validate_password_policy=MEDIUM; 
set global validate_password_length=10;
use mysql;
update user set host = '%' where user = 'root' and host not in ('127.0.0.1','::1') and host = 'localhost';
delete from user where user = 'root' and host not in ('127.0.0.1','::1','%');
update user set authentication_string=PASSWORD('nhapis_mysql_password') where user = 'root';
FLUSH PRIVILEGES;