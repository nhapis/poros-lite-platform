# !/bin/sh
 
td_str=''

#读取配置文件
function ReadConfig(){
	while read line
	do
		if [  -z `echo ${line} | grep "^\["` ];then
			eval "${line}"
		fi
	done < config.ini
}
 
function create_html_head(){
  echo -e "<!DOCTYPE HTML>
    <html>
    <head>
	<meta charset='"'utf-8'"'>
		<meta http-equiv='"'Content-Type'"' content='"'text/html; charset=utf-8'"' />
	</head>
	<body>
      <h1>$LOG_FILE</h1>"
}
 
function create_table_head(){
  echo -e "<table border="0">"
}
 
function create_td(){
#  if [ -e ./"$1" ]; then
    #echo $1
    td_str=`echo $1 | awk 'BEGIN{FS="|"}''{i=1; while(i<=NF) {print "<td>"$i"</td>";i++}}'`
    #echo $td_str
#  fi
}
 
function create_tr(){
  create_td "$1"
  echo -e "<tr>
    $td_str
  </tr>" >> $file_output
}
 
function create_table_end(){
  echo -e "</table>"
}
 
function create_html_end(){
  echo -e "</body></html>"
}
 
 
function create_html(){
  file_output=${POROS_DIR}/nginx/html/index.html
  rm -rf $file_output
  touch $file_output
 
  create_html_head >> $file_output
  create_table_head >> $file_output
 
  while read line
  do
    echo $line
    create_tr "$line"
  done < $LOG_FILE
 
  create_table_end >> $file_output
  create_html_end >> $file_output
}

ReadConfig
 
create_html
