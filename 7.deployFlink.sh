#!/bin/bash

ERROR_CODE=10
SUCCESS_CODE=0

#打印日志函数
function log_output(){

	DATE=`date "+%Y-%m-%d %H:%M:%S"`
	echo ${DATE} $1
	echo ${DATE} $1 >> ${LOG_FILE}
	return ${SUCCESS_CODE}
}

#读取配置文件
function ReadConfig(){
	while read line
	do
		if [  -z `echo ${line} | grep "^\["` ];then
			eval "${line}"
		fi
	done < config.ini
}

function DeployNginx(){

	mkdir -p ${POROS_DIR} > /dev/null 2>&1
	log_output 'Flink-1.10.2 部署目录：'${POROS_DIR}/flink-1.10.2
	log_output 'Flink 任务内存配置：'${FLINK_TASKMANAGER_JVM}
	log_output 'Flink 任务通道配置：'${FLINK_SLOTS}
	tar -xvf ./poros-litePackage/flink-1.10.2-scala_2.11_nhApis.tar.gz -C ${POROS_DIR}/  >  /dev/null 2>&1
	rm -rf ${POROS_DIR}/flink-1.10.2-scala_2.11_nhApis.tar.gz > /dev/null 2>&1
	scp -r ./poros-conf/flink/conf/flink-conf.yaml ${POROS_DIR}/flink-1.10.2/conf/ > /dev/null 2>&1
	sed -i "s#nhApis_FLINK_TASKMANAGER_JVM#${FLINK_TASKMANAGER_JVM}#g" ${POROS_DIR}/flink-1.10.2/conf/flink-conf.yaml
	sed -i "s#nhApis_FLINK_SLOTS#${FLINK_SLOTS}#g" ${POROS_DIR}/flink-1.10.2/conf/flink-conf.yaml
	log_output "Flink 启动脚本: /poros/tools/flink-1.10.2/bin/start-cluster.sh"
	/poros/tools/flink-1.10.2/bin/start-cluster.sh
	log_output "Flink Web访问地址: http://${WORK_IP}:28801"
}

function main()
{
	
	echo -e "\033[1m"
 	#读取配置文件
    ReadConfig
	
	log_output '-----------------------------------------------'

	echo -e "\033[31m正在部署 \033[33mFlink-1.10.2,请稍后...\033[1m"
		
	#安装Tengine
	DeployNginx
	
	echo -e "\033[31m部署完成 \033[32mFlink-1.10.2部署完成. \033[1m"

	log_output '-----------------------------------------------'
	
	echo -e "\033[0m"
}

main
