#!/bin/bash

ERROR_CODE=10
SUCCESS_CODE=0

#打印日志函数
function log_output(){

	DATE=`date "+%Y-%m-%d %H:%M:%S"`
	echo ${DATE} $1
	echo ${DATE} $1 >> ${LOG_FILE}
	return ${SUCCESS_CODE}
}

#读取配置文件
function ReadConfig(){
	while read line
	do
		if [  -z `echo ${line} | grep "^\["` ];then
			eval "${line}"
		fi
	done < config.ini
}

#安装ES环境
function InstallES(){

	ES_TOOLS_BASE_PATH=${POROS_DIR}/elasticsearch-7.3.2
	
	mkdir -p ${POROS_DIR} > /dev/null 2>&1
	mkdir -p ${ES_DATA_PATH} > /dev/null 2>&1
	mkdir -p ${ES_TOOLS_BASE_PATH} > /dev/null 2>&1
		
	scp -r ./poros-litePackage/elasticsearch-7.3.2_nhApis.tar.gz ${POROS_DIR}/ > /dev/null 2>&1
	tar -xvf ${POROS_DIR}/elasticsearch-7.3.2_nhApis.tar.gz -C ${POROS_DIR}/ > /dev/null 2>&1
	scp -r ${ES_TOOLS_BASE_PATH} ${ES_TOOLS_BASE_PATH}-node-1 > /dev/null 2>&1
	scp -r ${ES_TOOLS_BASE_PATH} ${ES_TOOLS_BASE_PATH}-node-2 > /dev/null 2>&1
	scp -r ${ES_TOOLS_BASE_PATH} ${ES_TOOLS_BASE_PATH}-node-3 > /dev/null 2>&1
	rm -rf ${POROS_DIR}/elasticsearch-7.3.2_nhApis.tar.gz > /dev/null 2>&1
	rm -rf ${ES_TOOLS_BASE_PATH} > /dev/null 2>&1
	
	log_output 'ElasticSearch-Node1 部署目录：'${ES_TOOLS_BASE_PATH}-node-1
	log_output 'ElasticSearch-Node1 数据目录：'${ES_DATA_PATH}/node1
	log_output 'ElasticSearch-Node2 部署目录：'${ES_TOOLS_BASE_PATH}-node-2
	log_output 'ElasticSearch-Node2 数据目录：'${ES_DATA_PATH}/node2
	log_output 'ElasticSearch-Node3 部署目录：'${ES_TOOLS_BASE_PATH}-node-3
	log_output 'ElasticSearch-Node3 数据目录：'${ES_DATA_PATH}/node3
	log_output 'ElasticSearch 快照备份目录：'${ES_BACKUP_PATH}

	scp -r ./poros-conf/elasticsearch/config/elasticsearch-1.yml ${ES_TOOLS_BASE_PATH}-node-1/config/elasticsearch.yml > /dev/null 2>&1
	scp -r ./poros-conf/elasticsearch/config/elasticsearch-2.yml ${ES_TOOLS_BASE_PATH}-node-2/config/elasticsearch.yml > /dev/null 2>&1
	scp -r ./poros-conf/elasticsearch/config/elasticsearch-3.yml ${ES_TOOLS_BASE_PATH}-node-3/config/elasticsearch.yml > /dev/null 2>&1
	scp -r ./poros-conf/elasticsearch/config/jvm.options ${ES_TOOLS_BASE_PATH}-node-1/config/jvm.options > /dev/null 2>&1
	scp -r ./poros-conf/elasticsearch/config/jvm.options ${ES_TOOLS_BASE_PATH}-node-2/config/jvm.options > /dev/null 2>&1
	scp -r ./poros-conf/elasticsearch/config/jvm.options ${ES_TOOLS_BASE_PATH}-node-3/config/jvm.options > /dev/null 2>&1
	
	mkdir -p ${ES_BACKUP_PATH}
	
	sed -i "s#nhApis_ES_DATA_PATH#${ES_DATA_PATH}#g" ${ES_TOOLS_BASE_PATH}-node-1/config/elasticsearch.yml
	sed -i "s#nhApis_ES_BACKUP_PATH#${ES_BACKUP_PATH}#g" ${ES_TOOLS_BASE_PATH}-node-1/config/elasticsearch.yml
	mkdir -p ${ES_DATA_PATH}/node1
	sed -i "s#nhapis_ES_MEM#${ES_MEM}#g" ${ES_TOOLS_BASE_PATH}-node-1/config/jvm.options
	sed -i "s#nhApis_ES_DATA_PATH#${ES_DATA_PATH}#g" ${ES_TOOLS_BASE_PATH}-node-2/config/elasticsearch.yml
	sed -i "s#nhApis_ES_BACKUP_PATH#${ES_BACKUP_PATH}#g" ${ES_TOOLS_BASE_PATH}-node-2/config/elasticsearch.yml
	mkdir -p ${ES_DATA_PATH}/node2
	sed -i "s#nhapis_ES_MEM#${ES_MEM}#g" ${ES_TOOLS_BASE_PATH}-node-2/config/jvm.options
	sed -i "s#nhApis_ES_DATA_PATH#${ES_DATA_PATH}#g" ${ES_TOOLS_BASE_PATH}-node-3/config/elasticsearch.yml
	sed -i "s#nhApis_ES_BACKUP_PATH#${ES_BACKUP_PATH}#g" ${ES_TOOLS_BASE_PATH}-node-3/config/elasticsearch.yml
	mkdir -p ${ES_DATA_PATH}/node3
	sed -i "s#nhapis_ES_MEM#${ES_MEM}#g" ${ES_TOOLS_BASE_PATH}-node-3/config/jvm.options
	
	log_output 'ElasticSearch 用户/用户组：elsearch:elsearch'
	groupadd elsearch;useradd elsearch -g elsearch;echo 'elasticsearch' | passwd --stdin elsearch
	chown -R elsearch:elsearch ${ES_TOOLS_BASE_PATH}-node-1
	chown -R elsearch:elsearch ${ES_TOOLS_BASE_PATH}-node-2
	chown -R elsearch:elsearch ${ES_TOOLS_BASE_PATH}-node-3
	chown -R elsearch:elsearch ${ES_DATA_PATH}/node1
	chown -R elsearch:elsearch ${ES_DATA_PATH}/node2
	chown -R elsearch:elsearch ${ES_DATA_PATH}/node3
	
	chown -R elsearch:elsearch ${ES_BACKUP_PATH}
	
	log_output 'ElasticSearch-Node1 启动脚本：su - elsearch -c ''"'"${ES_TOOLS_BASE_PATH}-node-1/bin/elasticsearch -d"'"'
	su - elsearch -c "${ES_TOOLS_BASE_PATH}-node-1/bin/elasticsearch -d" > /dev/null 2>&1
	sleep 1
	log_output 'ElasticSearch-Node2 启动脚本：su - elsearch -c ''"'"${ES_TOOLS_BASE_PATH}-node-2/bin/elasticsearch -d"'"'
	su - elsearch -c "${ES_TOOLS_BASE_PATH}-node-2/bin/elasticsearch -d" > /dev/null 2>&1
	sleep 1
	log_output 'ElasticSearch-Node3 启动脚本：su - elsearch -c ''"'"${ES_TOOLS_BASE_PATH}-node-3/bin/elasticsearch -d"'"'
	su - elsearch -c "${ES_TOOLS_BASE_PATH}-node-3/bin/elasticsearch -d" > /dev/null 2>&1
	sleep 1
	log_output 'ElasticSearch Api端口：'${WORK_IP}':29030,'${WORK_IP}':29031,'${WORK_IP}':29032'
	log_output 'ElasticSearch Http端口：'${WORK_IP}':29020,'${WORK_IP}':29021,'${WORK_IP}':29022'
			
	return ${SUCCESS_CODE}
}

function main()
{
		echo -e "\033[1m"
		
		#读取配置文件
        ReadConfig
		
		log_output '-----------------------------------------------'
		
		echo -e "\033[31m正在部署 \033[33mElasticSearch-7.3.2,请稍后...\033[1m"

		#安装ES
		InstallES
		
		echo -e "\033[31m部署完成 \033[32mElasticSearch-7.3.2部署完成. \033[1m"
	
		log_output '-----------------------------------------------'
		
		echo -e "\033[0m"		
}

main

