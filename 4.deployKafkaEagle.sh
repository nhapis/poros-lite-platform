#! /bin/bash

ERROR_CODE=10
SUCCESS_CODE=0

#打印日志函数
function log_output(){

	DATE=`date "+%Y-%m-%d %H:%M:%S"`
	echo ${DATE} $1
	echo ${DATE} $1 >> ${LOG_FILE}
	return ${SUCCESS_CODE}
}

#读取配置文件
function ReadConfig(){
	while read line
	do
		if [  -z `echo ${line} | grep "^\["` ];then
			eval "${line}"
		fi
	done < config.ini
}


function Deploy(){

	KE_TOOLS_PATH=${POROS_DIR}/kafka-eagle-web-2.0.2
	
	log_output 'KafkaEagle 部署目录：'${KE_TOOLS_PATH}
	mkdir -p ${POROS_DIR} > /dev/null 2>&1
	mkdir -p ${KE_TOOLS_PATH} > /dev/null 2>&1
	scp -r ./poros-litePackage/kafka-eagle-web-2.0.2-bin.tar.gz ${POROS_DIR}/ > /dev/null 2>&1
	rm -rf ${KE_TOOLS_PATH} > /dev/null 2>&1
	tar -xvf ${POROS_DIR}/kafka-eagle-web-2.0.2-bin.tar.gz -C ${POROS_DIR}/ > /dev/null 2>&1
	rm -rf ${POROS_DIR}/kafka-eagle-web-2.0.2-bin.tar.gz > /dev/null 2>&1
	
	echo "export KE_HOME=$KE_TOOLS_PATH" >> /etc/profile
	echo "export PATH=$PATH:$KE_HOME/bin" >> /etc/profile
	source /etc/profile

	scp -r ./poros-conf/kafka-eagle/conf/system-config.properties ${KE_TOOLS_PATH}/conf/ > /dev/null 2>&1
	
	sed -i "s/nhapis_mysql_password/${MYSQL_PASS}/g" ${KE_TOOLS_PATH}/conf/system-config.properties
	sed -i "s/nhApis_MYSQL_PORT/${MYSQL_PORT}/g" ${KE_TOOLS_PATH}/conf/system-config.properties
	sed -i "s/nhApis_WORK_IP/${WORK_IP}/g" ${KE_TOOLS_PATH}/conf/system-config.properties
	log_output "KafkaEagle 启动脚本：${KE_TOOLS_PATH}/bin/ke.sh start"
	chmod +x ${KE_TOOLS_PATH}/bin/ke.sh
	sh ${KE_TOOLS_PATH}/bin/ke.sh start
	#LOCAL_IP=`hostname -I`
	#LOCAL_IP=`echo "${LOCAL_IP// /}"`
	log_output "KafkaEagle 访问地址：http://${WORK_IP}:8048"
	log_output "KafkaEagle 用户名/密码：admin/123456"
}

#主函数
function main(){

	echo -e "\033[1m"
			
	#读取配置文件
	ReadConfig
	
	log_output '-----------------------------------------------'
	
	echo -e "\033[31m正在部署 \033[33mkafka-eagle-web-2.0.2,请稍后...\033[1m"
		  
	
	Deploy
   
	echo -e "\033[31m部署完成 \033[32mkafka-eagle-web-2.0.2部署完成. \033[1m"
	
	log_output '-----------------------------------------------'
	
	echo -e "\033[0m"
}

main

