#! /bin/bash

ERROR_CODE=10
SUCCESS_CODE=0

#打印日志函数
function log_output(){

	DATE=`date "+%Y-%m-%d %H:%M:%S"`
	echo ${DATE} $1
	echo ${DATE} $1 >> ${LOG_FILE}
	return ${SUCCESS_CODE}
}

#读取配置文件
function ReadConfig(){
	while read line
	do
		if [  -z `echo ${line} | grep "^\["` ];then
			eval "${line}"
		fi
	done < config.ini
}

function openFirewall(){
	systemctl enable firewalld.service
	service firewalld restart
	firewall-cmd --list-all
	firewall-cmd --permanent --add-port=80/tcp
	firewall-cmd --permanent --add-port=8048/tcp
	firewall-cmd --permanent --add-port=29902/tcp
	firewall-cmd --permanent --add-port=29903/tcp
	firewall-cmd --permanent --add-port=29904/tcp
	firewall-cmd --permanent --add-port=28801/tcp
	firewall-cmd --permanent --add-port=25610/tcp
	firewall-cmd --permanent --add-port=6669/udp
	firewall-cmd --reload
	firewall-cmd --list-all
}

#主函数
function main(){

	echo -e "\033[1m"
	
	#读取配置文件
	ReadConfig
	
	log_output '-----------------------------------------------'
	
	echo -e "\033[31m正在配置 \033[33m防火墙规则,请稍后...\033[1m"
		  
	openFirewall	  
		
	echo -e "\033[31m配置完成 \033[32m防火墙规则. \033[1m"
	
	echo -e "\033[0m"
}

main

