#!/bin/bash

LOCAL_WORK_PATH=`echo $( cd "$( dirname "${BASH_SOURCE[0]}" )/" && pwd )`


$LOCAL_WORK_PATH/1.installMysql.sh
$LOCAL_WORK_PATH/2.deployZookeeper.sh
$LOCAL_WORK_PATH/3.deployKafka.sh
$LOCAL_WORK_PATH/4.deployKafkaEagle.sh
$LOCAL_WORK_PATH/5.deployRedis.sh
$LOCAL_WORK_PATH/6.deployNginx.sh
$LOCAL_WORK_PATH/7.deployFlink.sh
$LOCAL_WORK_PATH/8.deployElasticSearch.sh
$LOCAL_WORK_PATH/9.deployKibana.sh
$LOCAL_WORK_PATH/showLog.sh

